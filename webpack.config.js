const path = require('path');
const merge = require('webpack-merge');
const CopyWebpackPlugin = require('copy-webpack-plugin');

const dev = require('./webpack.dev.config');
const prod = require('./webpack.prod.config');

const config = {
    context: path.resolve(__dirname, './src'),

    entry: {
        'youtube-ftw': './js/youtube-ftw.js',
        'settings': './js/settings.js',
    },

    output: {
        filename: './js/[name].js',
        sourceMapFilename: './js/[name].js.map',
    },

    watchOptions: {
        ignored: /node_modules/,
    },

    module: {
        rules: [{
            test: /\.m?js$/,
            exclude: /node_modules/,
            use: {
                loader: 'babel-loader',
                options: {
                    presets: ['@babel/preset-env'],
                }
            }
        }, {
            test: /\.svg$/,
            loader: 'svg-inline-loader'
        }]
    },

    plugins: [
        new CopyWebpackPlugin([
            { from: 'manifest.json' },
            { from: '**/*.html' },
            { from: '**/*.css' },
            { from: '**/*.png' },
        ]),
    ],
};

module.exports = (env, argv) => {
    if (argv.mode === 'development') {
        return merge(config, dev);
    }

    if (argv.mode === 'production') {
        return merge(config, prod);
    }
};

