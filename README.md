# Youtube ![FTW](/src/res/icon32.png)

A browser extension for **Firefox** and **Chrome**.
Open any youtube video to the full size of your tab window.

![Youtube FTW](/extras/Youtube_FTW_screenshot.png?raw=true)

## Installation

### Firefox
Get it from the [Mozilla add-ons store](https://addons.mozilla.org/en-US/firefox/addon/youtube-ftw/).

### Chrome-based browsers
Download the latest [release](https://gitlab.com/starmatt/youtube-ftw/-/releases), extract the zipfile to a new a folder and [import it in your browser](https://www.google.com/search?q=chrome+import+unpacked+extension).

### Build from source

If you don't have it, install [npm](https://www.npmjs.com/) on your machine.


Download the source and change directory:
```
git clone https://github.com/starmatt/youtube-ftw.git
cd youtube-ftw
```

Install dependencies and run the build command:
```
npm install
npm run build:prod
```

**Optional**: package the extension into a zipfile (requires the [web-ext](https://github.com/mozilla/web-ext) tool):

The zipfile will be located in the newly created `web-ext-artifacts` folder.
```
npm run pack
```

Import the `release/build` folder or the zipfile into your browser.

## Usage

This extension adds an extra button to the controls of a youtube video.

Clicking the FTW button, or pressing the 'y' key, will open the video to the full size of your browser tab window.
The video can be restored by clicking on the FTW button again or pressing the Escape key.

### Configuration

Configure the extension behaviour via the popup ![popup icon](/src/res/icon16.png).

![Youtube FTW Popup](/extras/Youtube_FTW_Popup_screenshot.png?raw=true)

#### Options

- **Autostart**: Turn on FTW mode automatically when opening a youtube video (toggle on/off)
- **Wheel control**: Scroll up at the top of the page to turn FTW mode on, scroll down to turn it off (toggle on/off)
- **Button position**: Change the position of the button among the other player controls

## License

MIT License (see LICENSE file)
