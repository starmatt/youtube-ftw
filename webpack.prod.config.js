const path = require('path');
const webpack = require('webpack');
const CopyWebpackPlugin = require('copy-webpack-plugin');

const config = {
    output: {
        path: path.resolve(__dirname, './release/build'),
    },

    plugins: [
        new webpack.DefinePlugin({
            'process.env.IS_PROD': true,
        }),

        new CopyWebpackPlugin([{
            from: 'node_modules/webextension-polyfill/dist/browser-polyfill.min.js',
            to: 'lib/browser-polyfill.js',
            context: '../',
            flatten: true,
            transform: function(content, path) {
                return content.toString().replace('//# sourceMappingURL=browser-polyfill.min.js.map', '')
            },
        }]),
    ],

    target: 'web',
    mode: 'production',
    devtool: 'none',
};

module.exports = config;
