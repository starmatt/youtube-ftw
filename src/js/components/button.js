module.exports = (function () {
    const button = document.createElement('button');

    button.classList.add('ftw-button', 'ytp-button');
    button.innerHTML = require('../../res/button.svg');
    button.title = 'Full tab window (y)';
    button.setAttribute('aria-label', 'Full tab window (y)');

    return button;
})();

