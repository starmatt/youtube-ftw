module.exports = (function() {
    const tooltip = document.createElement('div');
    const wrapper = document.createElement('div');
    const content = document.createElement('span');

    tooltip.classList.add('ftw-tooltip', 'ytp-tooltip', 'ytp-bottom');
    wrapper.classList.add('ytp-tooltip-text-wrapper');
    content.classList.add('ytp-tooltip-text');
    wrapper.appendChild(content);
    tooltip.appendChild(wrapper);
    tooltip.style.opacity = 0;
    tooltip.style.display = 'none';

    return tooltip;
})();

