'use strict';

const settings = {
    inputs: {
        autostart: document.querySelector('.ftw-set-autostart'),
        wheelcontrol: document.querySelector('.ftw-set-wheelcontrol'),
        positions: document.querySelectorAll('.ftw-set-position'),
    },

    settings: {
        autostart: 'off',
        wheelcontrol: 'on',
        position: 10,
    },

    statuses: {
        onFocus: document.querySelector('.status.on-focus'),
        onSave: document.querySelector('.status.on-save'),
    },

    statusText: {
        onFocus: 'Press <code>Enter</code> to confirm',
        onSave: '✓',
    },

    lastCustomPosition: null,

    timeout: null,

    handleToggles(event) {
        const input = event.target;

        input.value = input.value === 'off' ? 'on' : 'off';
        input.textContent = input.value;
        input.classList.toggle('on');
        this.save(input.dataset.setting, input.value);
    },

    validateCustomInput(event) {
        const allowedKeys = ['1', '2', '3', '4', '5', '6', '7', '8', '9', '0', 'Enter', 'Backspace', 'Delete', 'ArrowUp', 'ArrowDown', 'ArrowLeft', 'ArrowRight', 'Tab'];

        if (allowedKeys.indexOf(event.key) === -1) {
            return event.preventDefault();
        }

        setTimeout(() => {
            const input = event.target;
            let value = input.value === '' ? 0 : Number(input.value);

            if (value > 10) value = 10;
            if (event.key === 'ArrowUp') value += value < 10 ? 1 : 0;
            if (event.key === 'ArrowDown') value -= value > 0 ? 1 : 0;
            input.value = value <= 0 ? '' : value;

            if (event.key === 'Enter') {
                this.handlePosition(event);
            }
        }, 1);
    },

    handlePosition(event) {
        const input = event.target;

        if (input.value !== '' && input.value > 0) {
            if (event.type === 'keydown') {
                this.lastCustomPosition = input.value;
            }

            this.inputs.positions.forEach(item => item.classList.remove('on'));
            input.classList.add('on');
            this.save('position', Number(input.value));
        } else {
            input.value = this.lastCustomPosition;
        }
    },

    async save(setting, value) {
        await browser.storage.local.set({ [setting]: value });
        await this.applySettings();

        if (this.timeout !== null) {
            clearTimeout(this.timeout);

            this.statuses.onSave.style.color = '#fff';
            setTimeout(() => this.statuses.onSave.style.color = '', 200);
        }

        this.statuses.onSave.style.opacity = 1;
        this.statuses.onSave.innerHTML = this.statusText.onSave;

        this.timeout = setTimeout(() => {
            this.statuses.onSave.style.opacity = '';

            this.timeout = setTimeout(() => {
                this.statuses.onSave.innerHTML = '';
                this.timeout = null;
            }, 250);
        }, 2000);
    },

    registerEventListeners() {
        this.inputs.autostart.addEventListener('click', this.handleToggles.bind(this));
        this.inputs.wheelcontrol.addEventListener('click', this.handleToggles.bind(this));

        this.inputs.positions.forEach(input => {
            if (input.tagName === 'BUTTON') {
                input.addEventListener('click', this.handlePosition.bind(this))
            } else {
                input.addEventListener('focus', () => {
                    this.statuses.onFocus.style.opacity = 1;
                    this.statuses.onFocus.innerHTML = this.statusText.onFocus;
                });
                input.addEventListener('blur', event => {
                    this.statuses.onFocus.style.opacity = '';
                    setTimeout(() => this.statuses.onFocus.innerHTML = '', 250);

                    if (event.target.value === '') {
                        event.target.value = this.lastCustomPosition;
                    }
                });
                input.addEventListener('keydown', this.validateCustomInput.bind(this));
            }
        });
    },

    async applySettings() {
        const settings = await browser.storage.local.get();

        this.settings = {...this.settings, ...settings};
    },

    async init() {
        await this.applySettings();
        this.registerEventListeners();

        this.inputs.autostart.value = this.settings.autostart;
        this.inputs.autostart.textContent = this.settings.autostart;
        this.inputs.autostart.classList.add(this.settings.autostart);

        this.inputs.wheelcontrol.value = this.settings.wheelcontrol;
        this.inputs.wheelcontrol.textContent = this.settings.wheelcontrol;
        this.inputs.wheelcontrol.classList.add(this.settings.wheelcontrol);

        if (this.settings.position <= 1) {
            this.inputs.positions[0].classList.add('on');
        } else if (this.settings.position >= 10) {
            this.inputs.positions[2].classList.add('on');
        } else {
            this.inputs.positions[1].classList.add('on');
            this.inputs.positions[1].value = this.settings.position;
        }
    },
};

settings.init();

