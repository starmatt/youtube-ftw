'use strict';

const ftw = {
    button: require('./components/button'),

    tooltip: require('./components/tooltip'),

    doc: document.documentElement,

    baseURI: document.documentElement.baseURI,

    settings: {
        autostart: 'off',
        wheelcontrol: 'on',
        position: 10,
    },

    player: null,

    showTooltip() {
        const isFullscreen = this.player.classList.contains('ytp-fullscreen');
        let top, left;

        this.tooltip.style.display = '';
        this.tooltip.maxWidth = '300px';
        this.tooltip.children[0].children[0].textContent = this.button.getAttribute('title');

        top = this.player.offsetHeight - this.tooltip.offsetHeight - (isFullscreen ? 72 : 50);
        left = this.button.offsetLeft + this.button.offsetParent.offsetLeft + (this.button.offsetWidth / 2) - (this.tooltip.offsetWidth / 2);

        if (left >= this.player.offsetWidth - this.tooltip.offsetWidth) {
            left -= isFullscreen ? 66 : 46;
        }

        this.tooltip.style.top = top + 'px';
        this.tooltip.style.left = left + 'px';
        this.tooltip.style.opacity = '';
    },

    hideTooltip() {
        this.tooltip.style.opacity = 0;

        setTimeout(() => {
            this.tooltip.style.display = 'none';
            this.tooltip.style.top = 0;
            this.tooltip.style.left = 0;
            this.tooltip.maxWidth = '';
        }, 100);
    },

    handler(event) {
        if (['INPUT', 'SELECT', 'TEXTAREA'].indexOf(document.activeElement.tagName) >= 0) {
            return;
        }

        if (!this.doc.classList.contains('on')
            && (event.type === 'click' || event.keyCode === 89 || event.key === 'y')) {
            return this.open();
        }

        if (this.doc.classList.contains('on')
            && (event.type === 'click' || [27, 89].indexOf(event.keyCode) >= 0 || ['y', 'Escape'].indexOf(event.key) >= 0)) {
            return this.close();
        }

        if (event.type === 'wheel') {
            const direction = event.deltaY < 0;

            if (direction && this.doc.scrollTop === 0 && !this.doc.classList.contains('on')) {
                return this.open();
            } else if (!direction && this.doc.classList.contains('on')) {
                return this.close();
            };
        }
    },

    open(retry = false) {
        if (!this.player.isTheater()) {
            this.player.toggleTheater();
            this.player.theaterOldState = false;

            // Was theater mode toggled on properly? Retry if not
            if (!this.player.isTheater()) {
                return setTimeout(() => this.open(true), 50);
            }
        } else if (this.player.isTheater()) {
            this.player.theaterOldState = !retry;
        }

        this.doc.scrollTop = 0;
        this.doc.classList.add('on');
        window.dispatchEvent(new Event('resize'));
    },

    close() {
        if (this.player.isTheater() && !this.player.theaterOldState) {
            this.player.toggleTheater();
        }

        this.doc.scrollTop = 0;
        this.doc.classList.remove('on');
        window.dispatchEvent(new Event('resize'));
    },

    registerEventListeners() {
        if (this.settings.wheelcontrol === 'on') {
            window.addEventListener('wheel', this.handler.bind(this));
        }

        window.addEventListener('keydown', this.handler.bind(this));
        window.addEventListener('resize', this.hideTooltip.bind(this));
        this.button.addEventListener('click', this.handler.bind(this));
        this.button.addEventListener('mouseover', event => {
            this.showTooltip();
            event.target.removeAttribute('title');
        });
        this.button.addEventListener('mouseout', event => {
            this.hideTooltip();
            event.target.title = 'Full tab window (y)';
        });
    },

    initObservers() {
        // Watch for changes in player controls
        new MutationObserver(mutations => {
            mutations.forEach(mutation => {
                if (mutation.target.tagName === 'BUTTON' && !mutation.target.classList.contains('ftw-button')) {
                    this.updateButtonPosition();
                }
            });
        }).observe(this.player.controls, { attributes: true, subtree: true });

        // Watch for changes in player location
        new MutationObserver(mutations => {
            mutations.forEach(mutation => {
                if (mutation.target.baseURI !== this.baseURI) {
                    this.baseURI = mutation.target.baseURI;

                    if (this.settings.autostart === 'on') {
                        this.open();
                    }
                }
            });
        }).observe(this.player, { attributes: true });
    },

    updateButtonPosition() {
        const children = Array.prototype.filter.call(this.player.controls.children, child => child.style.display !== 'none');

        this.player.controls.insertBefore(this.button, children[this.settings.position - 1]);
    },

    async getSettings() {
        const settings = await browser.storage.local.get();

        return {...this.settings, ...settings};
    },

    async init() {
        const player = document.querySelector('#movie_player');

        if (player && !this.doc.classList.contains('ftw-fullsize')) {
            this.player = player;
            this.player.controls = player.querySelector('.ytp-right-controls');
            this.player.isTheater = () => !!document.querySelector('[theater]');
            this.player.toggleTheater = () => document.querySelector('.ytp-size-button').click();
            this.player.theaterOldState = false;
            this.player.insertBefore(this.tooltip, this.player.children[9]);

            this.settings = await this.getSettings();
            this.updateButtonPosition();
            this.registerEventListeners();
            this.initObservers();
            this.doc.classList.add('ftw-fullsize');

            if (this.settings.autostart === 'on') {
                this.open();
            }

            return true;
        }

        return false;
    },
};

new MutationObserver((mutations, observer) => {
    const init = mutations.some(mutation => {
        if (mutation.target.id === 'movie_player') {
            return ftw.init();
        }
    });

    if (init) {
        observer.disconnect();
    }

}).observe(document, { attributes: true, subtree: true });

