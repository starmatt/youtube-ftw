const path = require('path');
const webpack = require('webpack');
const CopyWebpackPlugin = require('copy-webpack-plugin');
const TerserPlugin = require('terser-webpack-plugin');

const config = {
    output: {
        path: path.resolve(__dirname, './build'),
    },

    plugins: [
        new webpack.DefinePlugin({
            'process.env.IS_PROD': false,
        }),

        new CopyWebpackPlugin([{
            from: 'node_modules/webextension-polyfill/dist/browser-polyfill.js',
            to: 'lib/',
            context: '../',
            flatten: true,
            transform: function(content, path) {
                return content.toString().replace('//# sourceMappingURL=browser-polyfill.js.map', '')
            },
        }]),
    ],

    optimization: {
        minimizer: [
            new TerserPlugin({
                cache: true,
                parallel: true,
                sourceMap: true,
                terserOptions: {
                    ecma: 8,
                    compress: {
                        drop_console: true,
                    },
                },
            }),
        ],
    },

    target: 'web',
    mode: 'development',
    devtool: 'inline-source-map',
};

module.exports = config;
